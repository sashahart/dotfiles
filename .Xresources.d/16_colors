! Shared definitions for how to handle ANSI color codes.
! Putting shared definitions here lets them be used by various terminals, etc.
! - and also differ in version control without edits to other configs.

!!!! THESE ARE GOOD
! fg=30m bg=40m
! ansi art consistently assumes this is same black as background,
! so it really cant be very bright at all.
#define color_black_dim #000000

! 1;30m
#define color_black_bright #787878
! #404040

! fg=31m bg=41m
! #bf0000
#define color_red_dim #ef0303 

! bright 1;31m
! desaturated like VGA palette
! #ffa687 #ffa699 #ff7d6a #ff9080
#define color_red_bright #ffa699

! yellow
! dim fg=33m bg=43m
! CIECAM02 midpoint: #b56f00 #bf640a
#define color_yellow_dim #9f5308

! bright 1;33m #ffff40
#define color_yellow_bright #ffff20

! green
! dim fg=32m bg=42m
! #0ebf70 #067b00
#define color_green_dim #0f9b01
! bright 1;32m
! #13fe95 #13fe13 #94fe13
#define color_green_bright #3cff0b

! cyan
! dim fg=36m bg=46m
! #00747f #089493 #17b3a2 #30bfbf #40bfbf
#define color_cyan_dim #40bfbf
! bright 1;36m
! #05e4ff #3dfdfd #3dffff #40ffff  
#define color_cyan_bright #55ffff

! blue
! dim fg=34m bg=44m.
! pure rgb blue is hue=240. at 75% l, its pretty purple
! #3d07bf #1d84f8 #187bff #1c62f2 #1c62ff
#define color_blue_dim #154abf
! bright 1;34m #c2d6fe #bfd5ff #80abff 
#define color_blue_bright #6096ff

! magenta
! dim fg=35m bg=45m
! #c51cfc #b51ff9
#define color_magenta_dim #a21cdf
! bright 1;35m
! #fe04f8 #ff00fc #ff40fd #ff80fd
#define color_magenta_bright #ff40fd 

! normal white ~ light gray, dim fg=37m bg=47m
#define color_white_dim #b4b4b4
! bright white 1;37m
#define color_white_bright #f0f0f0

! default is black on white. invert that to be less eye-scorching.
! ansi codes tend to be used assuming black is the terminal background
#define color_background color_black_dim
#define color_foreground color_white_bright

!   ! red
!   ! dim fg=31m bg=41m
!   XTerm.VT100.color1: #800101
!   ! bright 1;31m
!   XTerm.VT100.color9: #fe0201
!   
!   ! yellow
!   ! dim fg=33m bg=43m
!   XTerm.VT100.color3: #7f6b06
!   ! bright 1;33m
!   XTerm.VT100.color11: #ffd60c
!   
!   ! green
!   ! dim fg=32m bg=42m
!   XTerm.VT100.color2: #0a7f4b
!   ! bright 1;32m
!   XTerm.VT100.color10: #13fe95
!   
!   ! cyan
!   ! dim fg=36m bg=46m
!   XTerm.VT100.color6: #027280
!   ! bright 1;36m
!   XTerm.VT100.color14: #05e4ff
!   
!   ! blue
!   ! dim fg=34m bg=44m
!   XTerm.VT100.color4: #290580
!   ! bright 1;34m
!   XTerm.VT100.color12: #5109fd
!   
!   ! magenta
!   ! dim fg=35m bg=45m
!   XTerm.VT100.color5: #7f027c
!   ! bright 1;35m
!   XTerm.VT100.color13: #fe04f8
!   
!   ! light gray, dim fg=37m bg=47m
!   XTerm.VT100.color7: #b4b4b4
!   ! white, bright 1;37m
!   XTerm.VT100.color15: #f0f0f0

!    ! dark gray, fg=30m bg=40m
!    XTerm.VT100.color0: #3c3c3c
!    ! middle gray, bright 1;30m
!    XTerm.VT100.color8: #787878
!    
!    ! red
!    ! dim fg=31m bg=41m
!    XTerm.VT100.color1: #a00000
!    ! bright 1;31m
!    XTerm.VT100.color9: #f00000
!    
!    ! yellow
!    ! dim fg=33m bg=43m
!    XTerm.VT100.color3: #aa8900
!    ! bright 1;33m
!    XTerm.VT100.color11: #ffce00
!    
!    ! green
!    ! dim fg=32m bg=42m
!    XTerm.VT100.color2: #00aa51
!    ! bright 1;32m
!    XTerm.VT100.color10: #00ff7a
!    
!    ! cyan
!    ! dim fg=36m bg=46m
!    XTerm.VT100.color6: #00a3aa
!    ! bright 1;36m
!    XTerm.VT100.color14: #00f4ff
!    
!    ! blue
!    ! dim fg=34m bg=44m
!    XTerm.VT100.color4: #1300aa
!    ! bright 1;34m
!    XTerm.VT100.color12: #1c00ff
!    
!    ! magenta
!    ! dim fg=35m bg=45m
!    XTerm.VT100.color5: #8900aa
!    ! bright 1;35m
!    XTerm.VT100.color13: #cd00ff
!    
!    ! light gray, dim fg=37m bg=47m
!    XTerm.VT100.color7: #b4b4b4
!    ! white, bright 1;37m
!    XTerm.VT100.color15: #f0f0f0


! evenly dividing hue space, starting at red
! V    red     yellow  green   cyan    blue    magenta
! 100% #fe0201 #ffd60c #13fe95 #05e4ff #5109fd #fe04f8
! 75%  #bf0201 #bfa109 #0ebf70 #04abbf #3d07bf #bf03bb
! 50%  #800101 #7f6b06 #0a7f4b #027280 #290580 #7f027c
