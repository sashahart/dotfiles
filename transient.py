"""Update the symlinks in ~/transient
"""
import os
import sys
import argparse
import logging
import shutil
from pathlib import Path
from itertools import chain
import yaml


class Group:
    def __init__(self, name, program_names):
        self.name = name
        self.program_names = program_names
        self.programs = None

    def file(self, key):
        """Give the specified File
        """
        for program in self.programs:
            print("group", self.name, program)
        # return self._files.get(key)

class Program:
    def __init__(self, name, file_names):
        self.name = name
        self.file_names = file_names
        self.files = None
        self.group_names = None
        self.groups = None


class File:
    def __init__(self, name, tags, program_names):
        self.name = name
        self.tags = tags
        self.program_names = program_names
        self.programs = None


class Everything:
    """Represent what is known about groups and programs.
    """

    class ParseError(Exception):
        """raised instead of returning invalid data from a parse.
        """

    def __init__(self, groups, programs, blacklist, destinations):
        self._files = None
        self._programs = None
        self._groups = None
        self._build(groups, programs)
        self.blacklist = set(blacklist)
        self.destinations = destinations

    def _build(self, group_data, program_data):
        """Heavy lifting of interpreting the contents of the yaml file.
        """

        groups = {}
        programs = {}
        files = {}

        last = None
        for index, (name, record) in enumerate(program_data.items()):
            if record is not None:
                if not isinstance(record, dict):
                    raise self.ParseError(
                        f"record is not a dict: {record} "
                        f"at index {index} "
                        f"last {last}"
                    )
                for file_name, tag_string in record.items():
                    tags = tag_string.split() if tag_string else []
                    file = files.get(file_name)
                    if not file:
                        file = File(file_name, tags, [])
                        files[file_name] = file
                    file.program_names.append(name)
                    file.tags.extend(tags)

            file_names = list(record.keys()) if record else []
            program = Program(name, file_names)
            programs[name] = program
            last = name

        for name, program_names in group_data.items():
            group = Group(name, program_names)
            groups[name] = group
            group.programs = [
                programs[program_name]
                for program_name in program_names
            ]

        for group in groups.values():
            for program in group.programs:
                if program.groups is None:
                    program.groups = []
                program.groups.append(group)

        self._groups = groups
        self._programs = programs
        self._files = files

    def _build_groups(self, data):
        return {
            key: Group(key, [self.program(value) for value in values])
            for key, values in data.items()
        }

    def file(self, key):
        """Give the specified File
        """
        return self._files.get(key)

    def program(self, key):
        """Give the specified Program
        """
        return self._programs.get(key)

    def group(self, key):
        """Give the specified Group
        """
        return self._groups.get(key)

    def files(self):
        """Enumerate Files
        """
        return self._files.values()

    def programs(self):
        """Enumerate Programs
        """
        return self._programs.values()

    def groups(self):
        """Enumerate Groups
        """
        return self._groups.values()

    @classmethod
    def from_doc(cls, doc):
        groups = doc["program groups"]
        programs = doc["program files"]
        groups["games"] = list(doc["games"].keys())
        programs.update(doc["games"])
        destinations = doc["destinations"]
        return cls(
            groups=groups,
            programs=programs,
            blacklist=doc["blacklisted files"],
            destinations=destinations,
        )

    @classmethod
    def from_yaml_path(cls, path):
        with open(path, "rb") as stream:
            doc = yaml.safe_load(stream)
        return cls.from_doc(doc)


class Directory:
    """Represent a directory we scan.
    """

    def __init__(self, path):
        if isinstance(path, str):
            path = Path(path)
        self.path = path

    def __truediv__(self, path):
        return self.path / path

    def paths(self, pattern):
        return self.path.glob(pattern)

    def relative(self, path):
        prefix = str(self.path).rstrip("/") + "/"
        str_path = str(path)
        if str_path.startswith(prefix):
            return str_path[len(prefix):]
        return str_path


class Home(Directory):
    """Represent a home directory.
    """

    def normalize(self, path):
        str_path = self.relative(path)
        if not str_path.startswith("/"):
            str_path = "~/{0}".format(str_path)
        return str_path


def on_blacklist(everything, normalized):
    return (
        normalized in everything.blacklist
        or any(
            normalized.endswith("/" + item)
            for item in everything.blacklist
            if not item.startswith(("~", "/"))
        )
    )


def remove_dangling_symlinks(everything, home):
    for path in home.paths(".*"):
        normalized = home.normalize(path)
        found = everything.file(normalized)
        if not found:
            continue
        if not path.is_symlink():
            continue
        if path.exists():
            yield ("skip", path, "symlink is not broken")
            continue
        yield ("rm", path)


def relocate_home_files(everything, home, dest):
    """Specifies what should happen without doing it.

    The actual achievement of the steps yielded out is done by
    create_performer.
    """

    # find dotfiles directly under ~
    paths = home.paths(".*")

    # iterate over them
    for path in paths:

        # # if it's already a symlink, ignore it
        if path.is_symlink():
            continue

        # get a filename we can use in the dest
        filename = home.relative(path)

        # get a path we can check against our database
        normalized = home.normalize(path)

        if on_blacklist(everything, normalized):
            yield ("blacklisted", normalized)
            continue

        # see if we know about this file in our database
        found = everything.file(normalized)
        # if not then skip it
        if not found:
            yield ("unrecognized", normalized)
            continue

        # enumerate the programs associated with this file
        programs = [
            everything.program(name)
            for name in found.program_names
        ]
        # enumerate the program groups associated with this file
        program_groups = [
            program.groups
            for program in programs
            if program.groups
        ]
        # if no groups, we'll just use the filename under our dir
        if not program_groups:
            dest_path = dest / filename
        # if groups, put it under dir named like the first group
        else:
            groups = list(chain(*program_groups))
            group = groups[0]
            dest_dir = dest / group.name
            dest_path = dest_dir / filename
            # make it if it doesn't exist
            if not dest_dir.exists():
                yield ("mkdir", dest_dir)
            # skip it if it exists but is not a directory,
            # otherwise the following moves etc. will be confusing
            elif not dest_dir.is_dir():
                yield ("skip", path, "couldn't make a parent directory")
                # don't do other checks
                continue

        if not path.exists():
            yield ("skip", path, "source doesn't exist")
        elif dest_path.exists():
            if dest_path.is_symlink():
                yield ("skip", dest_path, "already exists as symlink")
            elif dest_path.is_file():
                yield ("skip", dest_path, "already exists as file")
            # elif dest_path.is_directory():
            #     yield ("skip", dest_path, "already exists as directory")
            else:
                yield ("skip", dest_path, "already exists")
        elif path.is_dir() and "directory" not in found.tags:
            yield ("skip", path, "didn't expect a directory")
        elif path.is_file() and "file" not in found.tags:
            yield ("skip", path, "didn't expect a file")
        elif path.is_symlink() and "symlink" not in found.tags:
            yield ("skip", path, "didn't expect a symlink")
        else:
            yield ("mvln", path, dest_path)


def create_missing_symlinks(everything, home, dest):
    """Create symlinks in home for things in dest that aren't linked.

    Look in our dest for dotfiles.
    If there is not a corresponding symlink in home, make it.
    """

    def handle(directory, pattern=".*", group=None):
        for subpath in directory.paths(pattern):
            # get a relative path
            filename = dest.relative(subpath)
            group_filename = directory.relative(subpath)

            # we don't symlink to symlinks
            if subpath.is_symlink():
                yield ("skip", subpath, "not symlinking to symlink")
                continue

            # get a home-normalized filename
            normalized = home.normalize(filename)
            # if it's blacklisted, don't even think about it
            if on_blacklist(everything, normalized):
                yield ("blacklisted", subpath)
                continue

            # see if we know about this file inside group
            if group and group.name:
                home_path = home / group_filename
                group_normalized = home.normalize(group_filename)
                found = everything.file(group_normalized)
                if not found:
                    yield ("unrecognized", home_path)
                    continue
            # see if we know about this file outside of group
            else:
                home_path = home / filename
                found = everything.file(normalized)
            # if not, then skip it
            if not found:
                yield ("unrecognized", subpath)
                continue
            # otherwise, we check that the type is what we know about
            elif subpath.is_dir() and "directory" not in found.tags:
                yield ("skip", path, "no move: didn't expect a directory")
                continue
            elif subpath.is_file() and "file" not in found.tags:
                yield ("skip", path, "no move: didn't expect a file")
                continue
            # then we make sure we're not clobbering the destination
            elif home_path.is_symlink() or home_path.exists():
                if home_path.is_symlink():
                    yield ("skip", home_path, "already exists as symlink")
                elif home_path.is_file():
                    yield ("skip", home_path, "already exists as file")
                # elif home_path.is_directory():
                #     yield ("skip", home_path, "already exists as directory")
                else:
                    yield ("skip", home_path, "already exists")
                continue
            yield ("link", subpath, home_path)

    # look for top-level group directories
    # there should be a limited number of these
    group_paths = []
    for path in dest.paths("*"):
        # don't look at dotfiles yet
        if str(path).startswith("."):
            continue
        # only interested in dirs for now
        if not path.is_dir():
            continue
        # get a relative path
        filename = dest.relative(path)
        # find a matching group
        group = everything.group(filename)
        if not group:
            continue
        # note this to explore later
        group_paths.append((group, path))

    for group, path in group_paths:
        group_dir = Directory(path)
        yield from handle(group_dir, group=group)

    yield from handle(dest, group=None)

#   for path in dest.paths("*"):

#       # enumerate the programs associated with this file
#       # TODO: only looking within the group though?
#       programs = [
#           everything.program(name)
#           for name in found.program_names
#       ]



def _fake_report(message):
    """Report function used when --quiet.
    """
    _ = message


def _real_report(message):
    """Report function used when not --quiet.
    """
    print(message)


def _check_move(log, source, dest):
    """Last-minute checks prior to a move.
    """
    # Don't even try writing anything outside home
    home = os.environ["HOME"]
    if not str(dest).startswith(home):
        log.debug("move dest is not under $HOME")
        return False
    # Don't overwrite anything at all, just to be careful.
    if dest.exists():
        log.debug("move dest exists")
        return False
    # Nothing to move, but spare us some error handling
    if not source.exists():
        log.debug("move source does not exist")
        return False
    return True


def _fake_move(log, source, dest):
    """Bogus move function used to suppress action for --dry-run.
    """
    if not _check_move(log, source, dest):
        return False
    return True


def _real_move(log, source, dest):
    """Move a file from one place to another.
    """
    if not _check_move(log, source, dest):
        return False
    log.debug("shutil.move %r %r", source, dest)
    try:
        shutil.move(source, dest)
    except shutil.Error as err:
        _ = err
        # str(err) could end with "already exists" or "into itself"
        log.exception("move error")
        return False
    return True


def _fake_link(log, source, dest):
    """Bogus link function used to suppress action for --dry-run.

    This doesn't actually simulate what would happen based on the possible
    failure of any prior move.
    """
    return True


def _real_link(log, source, dest):
    """Create a symlink at dest referring to source.
    """
    # Never overwrite anything, just to be careful.
    # This can happen if the move wasn't attempted. It would be bad to
    # overwrite a real file with an invalid symlink.
    if dest.exists():
        log.debug("link dest exists")
        return False
    # This can happen if the move didn't succeed, or if the file disappeared
    # recently - the symlink can still be made, it will just be invalid.
    if not source.exists():
        log.debug("link source does not exist")
        return False
    log.debug("%r.symlink_to(%r)", dest, source)
    dest.symlink_to(source)
    return True


def _fake_mvln(log, source, dest):
    if not _fake_move(log, source, dest):
        log.debug("fake move failed: %r -> %r", source, dest)
        return False
    if not _fake_link(log, source, dest):
        log.debug("fake link failed: %r -> %r", source, dest)
        return False
    return True


def _real_mvln(log, source, dest):
    # Try to move source to dest.
    if not _real_move(log, source, dest):
        log.error("move failed: %r -> %r", source, dest)
        # Normally expect if we fail then nothing happened.
        # but spam a lot if weird things are going on.
        if dest.exists():
            log.error("dest is there: %r", dest)
        if not source.exists():
            log.error("source is gone: %r", source)
        return False
    # Double-check that the expected thing has happened.
    # Though if not, there's nothing we can do to recover it,
    # at least we won't make a confusing symlink.
    if not dest.exists():
        log.error("dest does not exist after move: %r", dest)
        return False
    if source.exists():
        log.error("source still exists after move: %r", source)
        return False
    # The move appeared to succeed. Now we'll try to do the link.
    # If the link fails, things may be in a broken state we should try to fix.
    if not _real_link(log, dest, source):
        if not _real_move(log, dest, source):
            log.error("failed to move file back after failed link")
            return False
        log.info("moved file back after failed link")
        return False
    return True



def _check_rm(log, path):
    if not path.exists():
        log.error("path does not exist: %r", path)
        return False
    return True

def _fake_rm(log, path):
    if not _check_rm(log, path):
        return False
    return True


def _real_rm(log, path):
    if not _check_rm(log, path):
        return False
    path.unlink()
    return True


def create_performer(options, adviser):
    """Iterator to carry out steps specified by adviser.
    """

    report = _fake_report if options.quiet else _real_report

    if options.dry_run:
        move = _fake_move
        link = _fake_link
        mvln = _fake_mvln
        rm = _fake_rm
    else:
        move = _real_move
        link = _real_link
        mvln = _real_mvln
        rm = _real_rm

    log = logging.getLogger("performer")

    for verb, *args in adviser:
        if verb == "unrecognized":
            if options.verbose:
                report(f"noop: unrecognized file {args[0]}")
        elif verb == "blacklisted":
            if options.verbose:
                report(f"noop: blacklisted {args[0]}")
        elif verb == "skip":
            if options.verbose:
                report(f"skip: {args[1]}: {args[0]}")
        elif verb == "mkdir":
            path = args[0]
            report(f"mkdir: {args[0]}")
            if not options.dry_run:
                path.mkdir(parents=True)
            yield True
        elif verb == "mvln":
            report(f"mvln: {args[0]} -> {args[1]}")
            # report(f"link: {args[0]} <- {args[1]}")
            yield mvln(log, *args)
        elif verb == "move":
            report(f"move: {args[0]} -> {args[1]}")
            yield move(log, *args)
        elif verb == "link":
            report(f"link: {args[1]} <- {args[0]}")
            yield link(log, *args)
        elif verb == "remove":
            report(f"rm  : {args[0]}")
            yield rm(log, *args)
        else:
            report(f"????: {verb} {args}")


def parse_environ(environ):
    """Get everything we want from the environment and check it.
    """
    environ_keys = ["HOME"]
    environ_vars = {}
    for key in environ_keys:
        value = environ.get(key)
        if not value:
            print(f"Missing environment variable: {key}")
            return 1
        environ_vars[key] = value
    return environ_vars


def parse_args(args):
    """Parse command line arguments.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--yaml", action="store", default="transient.yml")
    parser.add_argument("--quiet", action="store_true")
    parser.add_argument("--dry-run", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("--debug", action="store_true")
    parser.add_argument("--path", action="store")
    parser.add_argument("--dir-name", action="store", default="transient")
    options = parser.parse_args(args)
    return options


def _main(environ, args):
    """What happens when the script is run.
    """

    # Blanket precaution
    # This also saves us from having to put a million things in the blacklist
    if os.geteuid() == 0:
        print("Do not run this as root.")
        return 1

    environ_vars = parse_environ(environ)
    options = parse_args(args)
    if options.debug:
        logging.basicConfig(level=logging.DEBUG)
    report = _fake_report if options.quiet else _real_report
    everything = Everything.from_yaml_path(options.yaml)
    home = Home(Path.home())
    forbidden_paths = [
        Path(""),
        Path("/"),
    ]
    if options.path:
        # Bypass options.dir_name
        dest_path = Path(options.path).expanduser()
    elif options.dir_name:
        dest_path = home / options.dir_name
    else:
        report("fail: no value for either path or dir-name")
        return 1
    if not dest_path.exists():
        report(f"fail: dest does not exist: {dest_path}")
        return 1
    if dest_path in forbidden_paths:
        report(f"fail: forbidden path {dest_path}")
        return 1
    dest = Directory(dest_path)

    advisers = [
        remove_dangling_symlinks(everything, home),
        relocate_home_files(everything, home, dest_path),
        create_missing_symlinks(everything, home, dest),
    ]
    opcount = 0
    for adviser in advisers:
        performer = create_performer(options, adviser)
        status = 1
        for result in performer:
            opcount += 1
            if not result:
                status = 1
                break
        else:
            status = 0
        if status == 1:
            report("fail: aborting")
            break
    else:
        if opcount == 0:
            message = "no operations performed"
            # only return error status when something went wrong
            # if there's nothing for us to do, it's a good thing
            status = 0
        elif status == 1:
            message = "operations failed"
        else:
            message = "all operations succeeded"
        if options.dry_run:
            report(f"{message} [dry run]")
        else:
            report(message)

    return status


def main():
    """External entry point.

    Minimum logic required to isolate _main from globally shared state.
    """

    args = sys.argv[1:]
    status = _main(os.environ, args)
    sys.exit(status)


main()
