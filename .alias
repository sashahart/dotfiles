# Shell aliases.
# This file is not a script. It must be sourced to work.
# It should only contain portable aliases (which work in zsh and bash).
# Shell functions go elsewhere, e.g. ~/.zsh/autoload

# color codes; auto works with more/less
alias grep='grep --color=auto'

# human readable size, / at end of directories, color codes
alias ls='ls -hFv --color=auto'

# list directories
alias lsd='ls -d *(-/DN)'

alias syslog='tail -100f /var/log/syslog'

alias vp='vimpager'

# short alias to put things in trash (instead of rm)
alias tp='trash-put'

# confirm 3+ files and recursion
alias rm="rm -I"

# confirm overwrite
alias cp="cp -i"

# confirm overwrite
alias mv="mv -i"

# make a backup of destination file
alias mvb='mv --backup=t'

# fix DOS-style typo
alias cd..="cd .."

alias mute="amixer set Master mute -q;  xset b off > /dev/null"
alias unmute="amixer set Master unmute -q; xset b on > /dev/null"
alias beepoff="amixer set Beep 0%; setopt NOBEEP; xset b off > /dev/null"
alias beepon="amixer set Beep 100%; setopt BEEP; xset b on > /dev/null" 
alias volume="amixer set Master -q"

alias task="python ~/tools/t/t.py --task-dir ~/tasks --list tasks"

alias noansi='sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"'
alias datehelp='date --help|sed "/^ *%a/,/^ *%Z/!d;y/_/!/;s/^ *%\([:a-z]\+\) \+/\1_/gI;s/%/#/g;s/^\([a-y]\|[z:]\+\)_/%%\1_%\1_/I"|while read L;do date "+${L}"|sed y/!#/%%/;done|column -ts_'
alias fbterm='FBTERM=1 fbterm'

# require 4-digit years,
# errors on tab inconsistencies, 
# don't write bytecode,
# warn on big 3.x incompatibilities,
# warn on first division which may do type coercion
# generate exception for any warning
alias pyd='PYTHONY2K="?" python -tt -B -3 -Qwarn -Werror'

# comes with pylint
alias pyr='pyreverse -o png'

# find what subdirectories have the most inodes.
alias inodes='find . -xdev -type f | cut -d "/" -f 2 | sort | uniq -c | sort -n'

alias vader='/usr/bin/vim -u ~/.vim/harness.vim -c :Vader!'
alias dc='docker-compose'
alias pep8="pycodestyle"
alias bugbear="flake8 --select=B001,B002,B003,B004,B005,B006,B007,B008,B009,B010,B301,B302,B303,B304,B305,B306,B902"
alias kc=kubectl
alias ansi='iconv -f CP437'
alias pipu='python3 -m pip install --user'
