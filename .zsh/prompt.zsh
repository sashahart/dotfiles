# evaluate variable references like ${vcs_info_msg_0_} at each prompt
# this also lets us use variables to define parameters like colors
setopt prompt_subst

# mark vcs_info for load; this fetches info from VCS for our prompt
autoload -Uz vcs_info

# figure out if we're in a container once
docker=""
[ -e /.dockerenv ] && docker=" %{%B%F{cyan}%}[docker]%{$reset_color%}"
is_root=0
[ $EUID -eq 0 ] && is_root=1

configure_vcs_info() {

    # Enable only git, can enable others when they are needed
    zstyle ':vcs_info:*' enable git

    # Check for working tree and staged changes, enabling %c and %u. Slow.
    zstyle ':vcs_info:*' check-for-changes true

    # set format string that vcs_info will use to generate its bit of prompt:
    # %s    which VCS is in use
    # %r    root dir of repo
    # %S    current path relative to root
    # %b    branch info, like master
    # %m    stash info
    # %u    unstaged changes
    # %c    staged changes
    # color codes will kind of work here - but they make it hard to compute the
    # visible length of strings, so to implement multiline prompts.
    zstyle ':vcs_info:git*' formats '%u %b '

    # actionformats is for special states like rebase or merge
    # %a    "actions" like merge or rebase
    zstyle ':vcs_info:git*' actionformats '(%a) %u %b'

}
configure_vcs_info

# this function is used to let multiline prompts work
prompt_width () {
    local zero='%([BSUbfksu]|([FK]|){*})'
    echo "${#${(S%%)1//$~zero/}}"
}

update_prompt() {
    # save the status of the last command
    local last_status="$?"

    # update version control info
    vcs_info

    # some prompt style parameters
    path_color="%F{green}"
    vcs_color="%B%F{white}"
    virtualenv_color="%F{white}"
    if [ $is_root -eq 0 ]; then
        user_color="%F{gray}"
    else
        user_color="%B%F{yellow}"
    fi
    host_color="%F{gray}"
    at_char=" @ "
    at_color="%F{red}"
    lcute=" "
    rcute=" "
    cute_color="%F{blue}"
    prompt_char=">"
    prompt_color="%B%F{white}"

    # define the prompt as separate left and right sections for lines
    declare -a lefts rights fillers
    # if the last command failed, then indicate that and give its exit code.
    if [ "$last_status" -ne 0 ]; then
        lefts+="%{%F{red}%}✘ %{%B%F{black}%}$last_status%{%b%}"
        fillers+=" "
        rights+=" "
    fi
    # show current path, followed by VCS info.
    # the output from vcs_info doesn't count properly for length if it contains
    # color codes, even escaped with %{%}. so we work around it here
    lefts+='%{$path_color%}%~ %{$vcs_color%}${vcs_info_msg_0_}%{%b%}'
    fillers+=" "

    # show prompt, indicating virtualenv and shell nesting level.
    virtualenv=$([ ! -z $VIRTUAL_ENV ] && basename $VIRTUAL_ENV)
    lefts+='%B%(!.%{%F{red}%}.%{$virtualenv_color%}$virtualenv%{%F{white}%})%{%f%b$prompt_color%}%(2L.$(repeat $(( $SHLVL - 1 )) printf "$prompt_char").)$prompt_char%{%f%b%} '
    fillers+=" "

    rights+="%{$cute_color%}$lcute%{%f%b$user_color%}%n%{%b${at_color}%}${at_char}%{%f%b$host_color%}%m%{%f%b%}$docker%{$cute_color%}$rcute"
    rights+=""
    
    local len_lefts len_rights len_lines
    len_lefts="${#lefts[@]}"
    len_rights="${#rights[@]}"
    len_lines=$(( len_lefts > len_rights ? len_lefts : len_rights ))

    declare -a lines
    local left right filler
    for i in {1..$len_lines} ; do
        left="${lefts[i]:- }"
        right="${rights[i]:- }"
        filler_char="${fillers[i]:- }"
        len_left=$(prompt_width "$left")
        len_right=$(prompt_width "$right")
        if [ "$i" -eq "$len_lines" ]; then
            lines+="$left"
            RPROMPT="$right"
        else
            len_filler=$(( COLUMNS - ( len_left + len_right ) ))
            if [[ $filler_width -lt 0 ]]; then
                filler_width=0
            fi
            # echo line $i: L=$len_left F=$len_filler R=$len_right
            filler="$(printf -- '$filler_char%.0s' $(seq $len_filler))"
            lines+="$left$filler$right"
        fi
    done
    # ANSI escape code to move cursor to bottom left before each prompt
    # this means you always look at the same place to type
    jump=""
    if [ ! -z "$JUMP_TO_LOWER_LEFT" ]; then
        jump="$JUMP_TO_LOWER_LEFT"
    fi
    # now we have an array of lines, join them on \n to make the prompt
    PROMPT="%{$jump%}${(j:
:)lines}"
}

set_prompt() {

    case "$1" in

        # fancy multiline prompt
        "fancy")
            configure_vcs_info
            autoload -Uz add-zsh-hook
            add-zsh-hook precmd update_prompt
            # update_prompt sets PROMPT/RPROMPT each time precmd runs
            ;;

        # simple one-line prompt with vcs info
        "simple")
            configure_vcs_info
            autoload -Uz add-zsh-hook
            add-zsh-hook precmd vcs_info
            PROMPT='%{%F{white}%}>%{$reset_color%} '
            RPROMPT='${vcs_info_msg_0_}%{$reset_color%}'
            ;;

        # A fairly typical one-line user@host prompt:
        # It works, but the cursor position varies so your eye has to scan to a
        # different position every time you change directories.
        # No need for $ vs # because the user is shown.
        "basic")
            PROMPT='%n@%m> '
            ;;

        # Minimal prompt which only does what is needed:
        # It tells you the shell is ready for a command and shows where you're
        # typing. Always in the same place. Use 'pwd' and 'whoami' as needed.
        "minimal")
            PROMPT='> '
            ;;
    esac

}

set_prompt fancy
