fasd_cache="$HOME/.fasd-init-cache"
orig_a=$(alias a)
if [[ "${commands[fasd]}" -nt "$fasd_cache" || ! -s "$fasd_cache"  ]]; then
    # removed posix-alias from this list, so we can control aliases.
    init_args=(zsh-hook zsh-ccomp zsh-ccomp-install zsh-wcomp zsh-wcomp-install)
    fasd --init "$init_args[@]" >! "$fasd_cache" 2> /dev/null
fi
source "$fasd_cache"
unset fasd_cache init_args
