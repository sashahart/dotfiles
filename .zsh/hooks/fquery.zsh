# A collection of find and ls wrappers for querying the filesystem in simpler
# language.

# specify here, just once, some file/directory names to exclude from 'ordinary
# file' queries
ignores=( .git .hg .svn .tox .mypy_cache node_modules )
ls_ignores=( --ignore=${^ignores} )
find_ignores=({-type,d,-name,${^ignores},-prune,-o})

# TODO: support color
LS_OPTS="--color=auto"

is_numeric () {
	if [[ -z "$1" ]];  then
		return 1
	elif [[ "$1" =~ ^[0-9]*$ ]]; then
		return 0
	fi
	return 1
}

alias _get_count='local count; if is_numeric "$1"; then count="$1"; shift; else count="-0" fi'
alias _count_limit='head -n "$count"'

# newest
# newest 5
# newest *.py
# newest 5 *.py
newest () {
	_get_count
	if [ "$#" -eq 0 ]; then
		ls "$LS_OPTS" -dtl $ls_ignores *(om[1,"$(($count > 0?$count:-1))"])
	else
		ls "$LS_OPTS" -dtl $@ | _count_limit
	fi
}

oldest () {
	_get_count
	if [ "$#" -eq 0 ]; then
        ls "$LS_OPTS" -drtl $ls_ignores *(Om[1,"$(($count > 0?$count:-1))"])
	else
		ls -dtl $@ | _count_limit
	fi
}

biggest () {
	_get_count
	if [ "$#" -eq 0 ]; then
        ls "$LS_OPTS" -dSl $ls_ignores *(-OL[1,"$(($count > 0?$count:-1))"])
	else
        ls "$LS_OPTS" -dSl $ls_ignores $@ | _count_limit
	fi
}

smallest () {
	_get_count
	if [ "$#" -eq 0 ]; then
        ls "$LS_OPTS" -drSl $ls_ignores *(oL[1,"$(($count > 0?$count:-1))"])
	else
		ls "$LS_OPTS" -drSl $ls_ignores $@ | _count_limit
	fi
}

# since 2010-01-01
# since yesterday
# since last monday
since () {
	rest="$@"
	find . $find_ignores -type f -newermt "$rest"
}

# between 2012-05-08 and 2012-05-10
between () {
	s="$@"
	a=("${(s/ and /)s}")
	echo "find . -type f -newermt \"$a[1]\" ! -newermt \"$a[2]\""
	find . $find_ignores -type f -newermt "$a[1]" ! -newermt "$a[2]"
}

# bigger 500M
bigger() {
	find . $find_ignores -type f -size "+$1"
}
# smaller 10k
smaller() {
	find . $find_ignores -type f -size "-$1"
}

# newer somefile.txt
newer() {
	find . $find_ignores -type f -newer "$1"
}

older() {
	find . $find_ignores -type f -older "$1"
}

like() {
	find . -iname "*$1*"
}

# TODO: tighter one
# writable executables
# not so normal characters

fishy() {
	# don't descend directories on other filesystems
	# don't crawl through device files etc.
	# any of the following:
	#	modified in future
	# 	no user or no group
	# 	world-writeable
	# 	setuid/setgid
	# 	filenames with weird unicode
	# and don't emit ignored filenames.
	find $1 \
		-xdev \
		-path '/proc' -prune -or \
		-path '/sys' -prune -or \
		-name '.gvfs' -prune -or \
		-newermt '1 second' -or \
		-nouser -o -nogroup -or \
		-regex '.*[^a-zA-Z 0-9/.,_~:+&="'"'"'^%*<{}()#@$;\[\]\\!-].*' -or \
		-type f -name "*.php" \
		-print
# TODO: fix permissions clauses!
#\( -perm -o+w -o \) -or \
#   	-type f -perm +g=s -or \
#   	-type f -perm +u=s -or \
}

# see stuff on path grouped by debian package:
# onpath | xargs dpkg -S | sort
# stuff on path without package
# onpath | xargs dpkg -S | grep "no path found matching" | sort
onpath () {
	print -l ${^path}/*(-*N)
}

# Find dupes that aren't in source control or other likely places
# the dance with dupes_prunes is to make the set of file types more DRY
dupes () {
    find $find_ignores -not -empty -type f -printf \"%s\n\" | sort -rn | uniq -d | xargs -I{} -n1 find $find_ignores -type f -size {}c -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=separate | cut -c 35-
}

directories () {
    ls "$LS_OPTS" -d *(-/DN)
}
