# have KUBECONFIG look at certain files first
# ~/.kube/config is really just for changing context
# ~/.kube/minikube-kubeconfig.yaml is for keeping minikube stuff out of other
# files.
export KUBECONFIG="${KUBECONFIG:+${KUBECONFIG}:}\
$HOME/.kube/config:\
$HOME/.kube/minikube-kubeconfig.yaml:\
"
