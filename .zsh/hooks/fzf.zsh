fzf_file="${XDG_CONFIG_HOME:-$HOME/.config}/fzf/fzf.zsh"
if [ -f "$fzf_file" ]; then
    source "$fzf_file"
else
    # newer style :/
    fzf_file="$HOME/.fzf.zsh"
    source "$fzf_file"
fi

if (( $+commands[rg] )) ; then
    export FZF_DEFAULT_COMMAND='rg --no-messages --files --no-ignore --hidden --glob "!*~" --glob "!.git/*"'
fi

pdf_open=xdg-open

# directory generator
__directories() {
    # locate -Ai -0 $@ | grep -z -vE '~$'
    fd -0 -td "$@"
}

# change to any directory
d() {
  local file
  if [ $# -le 0 ]; then
      file="$(__directories "$@" | fzf --read0 --exit-0 -1)"
  else
      file="$(__directories "$@" | fzf --read0 --exit-0 -1 -q "$@")"
  fi
  if [[ -n $file ]]
  then
     if [[ -d $file ]]
     then
        cd -- $file
     else
        cd -- ${file:h}
     fi
  fi
}

# edit any file
e() {
    # TODO: distinguish binaries and media
    local files
    IFS=$'\n' files=($(fzf --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

# open any file
o() {
    local files
    IFS=$'\n' files=($(fzf --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && xdg-open "${files[@]}"
}

# open file containing string
g() {
  local file
  local line

  search () {
      # --line-number has to be forced if not on a terminal
      rg --no-heading --hidden --line-number --column "$@"
  }
  read -r file line column <<<"$(search $@ | fzf -0 -1 | awk -F: '{print $1, $2, $3}')"
  if [[ -n $file ]] ; then
     vim $file "+call cursor($line, $column)"
  else
      return 1
  fi
  return 0
}

if (( $+commands[fast-p] )) ; then
p () {
    rg --files --type pdf \
    | fast-p \
    | fzf --read0 --reverse -e -q "$*" -d $'\t'  \
        --preview-window up:50% --preview '
            v=$(echo {q} | tr " " "|"); 
            echo -e {1}"\n"{2} | grep -E "^|$v" -i --color=always;
        ' \
    | cut -z -f 1 -d $'\t' | tr -d '\n' | xargs -r --null $pdf_open > /dev/null 2> /dev/null
}
fi

find_git_root() {
  local root
  root="$(git rev-parse --show-cdup)"
  if [ -z "$root" ]; then
      root="."
  fi
  echo "$root"
}

t() {
  local tagfile="$(find_git_root)/.tags"
  if [ ! -e "$tagfile" ]; then
      echo "tagfile $tagfile does not exist"
      return 1
  fi
  awk_program='BEGIN { FS="\t" } !/^!/ {print toupper($4)"\t"$1"\t"$2"\t"$3}' 
  local line
  line=$(
    awk "$awk_program" "$tagfile" | cut -c1-80 | fzf -q "$1" -i --nth=1,2
  ) && ${EDITOR:-vim} \
      $(cut -f3 <<< "$line") \
      -c "set nocst" \
      -c "silent tag $(cut -f2 <<< "$line")"
}

# change to directory using the fasd shell MRU.
if (( $+commands[fasd] )) ; then
unalias z 2> /dev/null
z() {
    [ $# -gt 0 ] && _fasd_cd "$*" && return
    local dir
    dir="$(fasd -Rdl "$1" | fzf -1 -0 --no-sort +m)" && cd "${dir}" || return 1
}
fi
