# How many commands to remember in memory.
HISTSIZE=100000
# Load command history from this file when the shell starts.
# if SAVEHIST > 0, this is also where history is saved to.
HISTFILE=~/.history
# How many history events to save to the history file on disk.
# 'for special reasons which I won't talk about, you should set $SAVEHIST to be
#  no more than $HISTSIZE'
# By default, nothing at all is saved until the shell exits.
SAVEHIST=90000
# Save timestamps in the history file.
setopt EXTENDED_HISTORY
# Consolidate blanks in command lines
setopt HIST_REDUCE_BLANKS
# Don't remember consecutive repeats
setopt HIST_IGNORE_DUPS
# Don't *show* duplicates in history search
setopt HIST_FIND_NO_DUPS
# Don't remember function definitions. (Save those to files)
setopt HIST_NO_FUNCTIONS
# Don't remember commands which start with a space.
setopt HIST_IGNORE_SPACE
# Ignore some commands for the purposes of saving to the histfile - they are
# still retained in memory until end of process.
# As a side effect, they are not shared with other shells.
# And as a result, they don't clutter history for other shells.
HISTORY_IGNORE="( |clear|reset|ls|bg|fg|cd|pwd|exit|tmsu)*"
# write to history file after commands run,
# rather than only when shell exits.
setopt INC_APPEND_HISTORY_TIME
