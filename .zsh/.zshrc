# $ZDOTDIR/.zshrc
# Sourced in interactive shells.
# Should contain commands to set up aliases, functions, options, keybindings, etc.

JUMP_TO_LOWER_LEFT=""
## Uncomment this to make the prompt always be at the bottom of the screen.
## This is also referenced in prompt.zsh
# JUMP_TO_LOWER_LEFT=$"\e[$LINES;0H"
printf "%b" "$JUMP_TO_LOWER_LEFT"

# Set this to 1 to enable (very slow) profiling of .zshrc as it's sourced
# logged to /tmp/startlog.*
PROFILE_ZSHRC=0
# Set this to 1 to enable internal reporting of how long zshrc takes to source.
TIME_ZSHRC=0
if [[ $TIME_ZSHRC -eq 1 ]]; then
	start_time=$(( $(( $(date +%s) * 1000000000 )) + $(date +%N) ))
fi
if [[ $PROFILE_ZSHRC -eq 1 ]]; then
	setopt prompt_subst
	PS4='$(date "+%s,%N"),%N,%i, '
	exec 3>&2 2>/tmp/startlog.$$
	setopt xtrace
fi
#zmodload zsh/zprof

p=/usr/share/kubectx/completion/kubectx.zsh
[ -f "$p" ] && fpath=( "$p" "${fpath[@]}" )

# Allow autoload from files in ~/.zsh/autoload,
# mark each one to be loaded at first use
# like aliases, these should be defined in every new shell
fpath=( "$HOME/.zsh/autoload" "${fpath[@]}" )

autoload_scripts=(~/.zsh/autoload/*)
for autoload_script in $autoload_scripts; do
	#echo "${autoload_script:t}"
	autoload -Uz ${autoload_script:t}
done
unset autoload_script; unset autoload_scripts

# Source files which contain hooks or keybindings (not aliases/functions).
# - stuff which doesn't need to be defined in every new shell (e.g., many
#   environment variables) can often be put in ~/.profile.
# - portable aliases should be in ~/.alias, shared across POSIX shells
# - zsh functions should be in ~/.zsh/autoload; these shouldn't, because
#   they may never be called by name and must be sourced eagerly.
# - these may change fpath
# - these must not run compinit (or complist won't work)
autoload -Uz add-zsh-hook
hook_scripts=(~/.zsh/hooks/*.zsh)
for hook_script in $hook_scripts; do
	[ -e "$hook_script" ] && source $hook_script
done
unset hook_scripts; unset hook_script

# Source single file for loose aliases/functions; anything which isn't
# a one-liner should move to autoload eventually.
# the same file is also sourced by bash automatically - keep it portable and
# you can use either freely, with your aliases.
# aliases need to be defined in every new shell, so zshrc is appropriate
[ -e ~/.alias ] && source ~/.alias

# Forcibly load complist before anything runs compinit,
# AND before the menuselect keymap may be used etc.
zmodload zsh/complist

# Define a simple script loader
load () {
    if [ -e "$1" ] ; then
        source "$1"
    else
        echo "no file to load: $1"
    fi
}

# zsh-specific settings modules.
# ls_colors.zsh can be skipped for speed if you don't change ~/.dircolors often,
# if dircolors is being used in ~/.profile anyway.
# but if ls_colors.zsh is used, it must run before completion.zsh
filenames=(keybindings terminal prompt history ls_colors completion syntax)
for filename in "$HOME/.zsh/"${^filenames}".zsh"; do
    load "$filename"
done
unset filename{,s}

# === Stuff below here can happen any time and can be moved to plugins. ===

# Compile .zcompdump for speedup - but compiling other things helps less
autoload zrecompile
zrecompile "$HOME/.zcompdump"

# The End.
if [[ $TIME_ZSHRC -eq 1 ]]; then
	end_time=$(( $(( $(date +%s) * 1000000000 )) + $(date +%N) ))
	duration=$(( $end_time - $start_time ))
	echo "startup took roughly $(( $duration / 1000000.0 ))ms"
	unset TIME_ZSHRC; unset start_time; unset end_time; unset duration
fi
if [[ $PROFILE_ZSHRC -eq 1 ]]; then
	unsetopt xtrace
	exec 2>&3 3>&-
	unset PROFILE_ZSHRC
fi

# TODO: move these...

# allow comments in interactive session, in case I'm recording or something - why not?
setopt INTERACTIVE_COMMENTS

# be careful with rm * or rm path/*
setopt RM_STAR_WAIT

if [ -e ~/.zsh/local.zsh ] ; then
    load ~/.zsh/local.zsh
else
    touch ~/.zsh/local.zsh
fi
