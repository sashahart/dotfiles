load /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
load ~/.zsh/repos/zsh-autosuggestions/zsh-autosuggestions.zsh

ZSH_AUTOSUGGEST_USE_ASYNC=1

# declare array so we can modify syntax highlighting styles
typeset -A ZSH_HIGHLIGHT_STYLES

# Tokens shell doesn't understand, like nonexistent commands.
ZSH_HIGHLIGHT_STYLES[unknown-token]=fg=red,bold

# Things that are pending, might be okay, but need more input to parse
ZSH_HIGHLIGHT_STYLES[back-quoted-argument-unclosed]=fg=yellow,bold
ZSH_HIGHLIGHT_STYLES[single-quoted-argument-unclosed]=fg=yellow,bold
ZSH_HIGHLIGHT_STYLES[double-quoted-argument-unclosed]=fg=yellow,bold
ZSH_HIGHLIGHT_STYLES[dollar-quoted-argument-unclosed]=fg=yellow,bold

# Tokens shell doesn't care about
ZSH_HIGHLIGHT_STYLES[comment]=fg=black,bold

# Things that parsed but aren't special
ZSH_HIGHLIGHT_STYLES[default]=fg=gray

# Special shell syntax
ZSH_HIGHLIGHT_STYLES[reserved-word]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[precommand]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[commandseparator]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[redirection]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[named-fd]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[assign]=fg=cyan,bold
# Use the same to find escape sequence CONSTANTS inside strings
ZSH_HIGHLIGHT_STYLES[rc-quote]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]=fg=cyan,bold
ZSH_HIGHLIGHT_STYLES[back-dollar-quoted-argument]=fg=cyan,bold

# Use magenta to find wild stuff
ZSH_HIGHLIGHT_STYLES[globbing]=fg=magenta,bold
ZSH_HIGHLIGHT_STYLES[history-expansion]=fg=magenta,bold

# Redefinables
# ZSH_HIGHLIGHT_STYLES[alias]=fg=yellow,bold
# ZSH_HIGHLIGHT_STYLES[suffix-alias]=fg=yellow,bold
# ZSH_HIGHLIGHT_STYLES[function]=fg=yellow,bold

# Valid head words for commands.
# Make these extra prominent - we want to know what we're saying to do.
# also provides default color for command, alias, function, builtin
# (which I barely see any point in distinguishing)
ZSH_HIGHLIGHT_STYLES[arg0]=fg=white,bold
# subtly distinguish the builtins for no reason
ZSH_HIGHLIGHT_STYLES[builtin]=fg=white,bold,underline

# Options for commands, -o or --option
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]=fg=white,bold
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]=fg=white,bold

# Filesystem paths.
# The problem is anything in the directory becomes this color,
# even if it's really invalid command word or something.
# ZSH_HIGHLIGHT_STYLES[path_prefix]=fg=green
# ZSH_HIGHLIGHT_STYLES[path]=fg=green,bold
# ZSH_HIGHLIGHT_STYLES[path_pathseparator]=fg=cyan,bold

