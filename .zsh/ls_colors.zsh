# Set up ls colors. This has to be before any use of $LS_COLORS (e.g. in
# setting the completion colors)
# if fancy version is not wanted, can use defaults or export LS_COLORS here
if [ $TERM != "dumb" ] && command -v dircolors > /dev/null ; then
    test -r $HOME/.dircolors && eval "$(dircolors -b $HOME/.dircolors)" || eval "$(dircolors -b)"
fi
