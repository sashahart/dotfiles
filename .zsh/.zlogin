# $ZDOTDIR/.zlogin
# Sourced only in login shells (e.g. tty, ssh)...
# Should contain commands that should be executed only in login shells.
# Should not contain alias definitions, options, environment variables etc.
# If you want this kind of thing but sourced before .zshrc, use .zprofile.
