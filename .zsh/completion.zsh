# complete from cursor position - zsh guide recommends it
setopt complete_in_word

# order menu across rows rather than down columns
setopt list_rows_first

# don't beep on ambiguous completion, it's not really an error
unsetopt list_beep

# Automatically list choices on ambiguous completion.
# This is set by default.
setopt auto_list

# When using tab completion, and there is more than one possible match,
# display the list of possibilities right away rather than waiting until tab
# was hit again.
# Explanation: listambiguous means "insert unambiguous prefix WITHOUT showing a
# list." unsetting that means "show a list when inserting unambiguous prefix."
unsetopt list_ambiguous

# When completing with tab,
# go ahead and insert the first option.
# this makes it unnecessary to unsetopt list_ambiguous.
setopt menu_complete

# don't strip slashes of names to directories or symlinks to dirs
# this reduces the chance e.g. of 'mv file dest' errors when dest does not
# exist
setopt no_auto_remove_slash

# Reference:
# :completion:function:completer:command:argument:tag
# * function is the name of the widget invoking the completion
# * completer is the name of the active completion function, w/o underscore
# * command is the command being completed
# * argument is a command argument or option, e.g. argumnt-1 or option-opt-1
# * tag discriminates some kinds of matches:
#   files directories local-directories manuals commands hosts jobs processes
#   signals tags urls groups users packages devices displays names? builtins
#   bookmarks options

# select from completion options with arrow keys.
# of course this prevents other uses of arrow keys, e.g. for history,
# but it only happens after a tab, and is visually indicated.
zstyle ':completion:*' menu select
# I want to use interactive (especially for things likely to have many options,
# like man pages) but it disables tab, so more keystrokes are always needed

# Use ls colors in path completion.
zstyle ':completion:*' list-colors "${(@s.:.)LS_COLORS}"
# if the colors here don't match ls, then it means LS_COLORS should have been
# set prior (e.g. by ~/.profile or after, see ./ls_colors.zsh)

# don't search old compctl completions, I never use them
zstyle ':completion:*' use-compctl false

# prefer exact matches, but then allow case-insensitive matches
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 

# Don't allow certain patterns to occur in _files matches - namely, backup
# files and ignoreable object/bytecode files.
# (however, these may still occur in default matches which run after _files,
# this just ensures they don't show up if there are other completions)
zstyle ':completion:*:(all-|)files' ignored-patterns \
	'*~' '*.(old|bak|swp|swo)' '*.(pyc|pyo|elc|zwc)' '*.(o|lo)'

# if file already mentioned in rm/kill/diff line, don't suggest it again
zstyle ':completion:*:(rm|kill|diff):*' ignore-line true

# default sort filenames by last inode change time.
zstyle ':completion:*' file-sort change

# don't offer current shell process as a completion unless given unambiguously.
zstyle ':completion:*:processes' ignored-patterns " #$$"

# process tree menu for process completion (kill, etc.)
# if the current call stack includes the completer for sudo, then don't make it
# specific to user. otherwise, do make it specific to user, to pull in stuff
# from other terminals. Need -e to do this kind of test at all.
zstyle -e ':completion:*:processes' command \
  'if (( $funcstack[(eI)$_comps[sudo]] )); ' \
      'then reply="ps -e --forest -o pid,user,cmd"; ' \
      'else reply="ps --forest -u $USER -o pid,cmd"; ' \
  'fi'
# same for names?
zstyle -e ':completion:*:processes-names' command \
  'if (( $funcstack[(eI)$_comps[sudo]] )); ' \
      'then reply="ps -e --forest -o cmd"; ' \
      'else reply="ps --forest -u $USER -o cmd"; '\
  'fi'

zstyle ':completion:*:*:killall:*' menu yes select

# nice colors for process completion
# TODO: replace with %F etc.?
autoload -Uz colors && colors
# =         start of zsh regex
# (#b)      enable backreferences
#  #        any amount of leading space (# works like regex * here)
# ([0-9]#)  group with any number of digits in it
#
# the second = in the whole expression is the default color.
# subsequent ones define colors for each backref.
# this gets more complicated because username is included in the readout for
# sudo kill, but not plain kill.
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#) #([A-Za-z][A-Za-z0-9\-_.]#)# #([\|\\_ ]# )([^ ]#)*=$color[blue]=$color[red]=$color[yellow]=$color[bold];$color[black]=$color[white]"

dunno () {
    # allow caching for very slow completions like dpkg/apt
    zstyle ':completion:*' use-cache on
    zstyle ':completion:*' cache-path ~/.zsh/cache

    # When completing after ../ don't put in parents of the entered path
    # and don't put in the current directory
    # TODO: don't limit this to cd?
    zstyle ':completion:*:cd:*' ignore-parents parent pwd
    zstyle ':completion:*:cd:*:directories' ignore-parents parent pwd ..
}

# This should be the only place compinit is loaded.
autoload -U compinit
compinit -u
