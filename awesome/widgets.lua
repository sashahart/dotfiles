-- Standard awesome library
require("awful")
require("vicious")		-- widgets
require("table")

browser = "firefox"
volapp = "gnome-control-center sound" -- or pavucontrol, etc.
taskapp = "gnome-system-monitor --show-processes-tab"
state = "TX"
city = "Austin"

widget_height = 20

widgets = {}

widgets.separator = widget({ type = "textbox" })
widgets.separator.text = " <span color='#666666'>::</span> "

widgets.net = widget({ type = "textbox" })
vicious.register(widgets.net, vicious.widgets.net, '↓<span color="#CC9393">${eth0 down_kb}</span> <span color="#7F9F7F">${eth0 up_kb}</span>↑', 3)

--netgraph = awful.widget.graph()
--netgraph:set_width(50)
--netgraph:set_height(widget_height)
--netgraph:set_background_color("#000000")
--netgraph:set_color("#FF5656")
--netgraph:set_gradient_colors({ "#FF5656", "#88A175", "#AECF96" })
--netgraph_t = awful.tooltip({ objects = { netgraph.widget },})
--vicious.cache(vicious.widgets.net)
--vicious.register(netgraph, vicious.widgets.net,
--                    function (widget, args)
--                        netgraph_t:set_text("down: " .. args["{eth0 down_kb}"] .. "kb" .. ", up: " .. args["{eth0 up_kb}"] .. "kb")
--                        return args["{eth0 down_kb}"]
--                    end)
--net = netgraph.widget


widgets.fs = widget({ type = "textbox" })
vicious.register(widgets.fs, vicious.widgets.fs, "/: ${/ used_p}%", 599)

widgets.vol = widget({ type = "textbox" })
widgets.vol.width = 40
vicious.register(widgets.vol, vicious.widgets.volume, " $1% ", 0.1, "PCM")
widgets.vol:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.util.spawn(volapp) end),
    awful.button({ }, 3, function () awful.util.spawn("amixer -q sset Master toggle")   end),
    awful.button({ }, 4, function () awful.util.spawn("amixer -q sset PCM 2dB+", false) end),
    awful.button({ }, 5, function () awful.util.spawn("amixer -q sset PCM 2dB-", false) end)
 ))

widgets.date = widget({ type = "textbox" })
vicious.register(widgets.date, vicious.widgets.date, "%b %d, %R", 60)
-- Go to Google Calendar on click
widgets.date:buttons(
	awful.util.table.join(
		awful.button({}, 1, function () awful.util.spawn ( 
			browser .. " http://calendar.google.com") end)))
-- Show view of month on mouseover
require('calendar2')
calendar2.addCalendarToWidget(widgets.date)

--widgets.mem = widget({ type = "textbox" })
--vicious.register(widgets.mem, vicious.widgets.mem, "$1% ($2MB/$3MB)", 13)

memgraph = awful.widget.progressbar()
memgraph:set_width(50)
memgraph:set_height(16)
memgraph:set_vertical(true)
memgraph:set_background_color("#494B4F")
memgraph:set_border_color(nil)
memgraph:set_color("#AECF96")
memgraph:set_gradient_colors({ "#AECF96", "#88A175", "#FF5656" })
vicious.register(memgraph, vicious.widgets.mem, "$1", 13)
-- widgets.mem = memgraph.widget

membar = awful.widget.progressbar()
membar:set_width(10)
membar:set_height(widget_height)
membar:set_vertical(true)
membar:set_background_color('#494B4F')
membar:set_color('#AECF96')
membar:set_gradient_colors({ '#AECF96', '#88A175', '#FF5656' })
membar_tooltip = awful.tooltip({ objects = { membar.widget },})
vicious.cache(vicious.widgets.mem)
vicious.register(membar, vicious.widgets.mem,
                function (widget, args)
                    membar_tooltip:set_text(" RAM: " .. args[2] .. "MB / " .. args[3] .. "MB ")
                    return args[1]
                 end, 13)
                 --update every 13 seconds
widgets.mem = membar.widget

--widgets.cpu = widget({ type = "textbox" })
--vicious.register(widgets.cpu, vicious.widgets.cpu, "$1%")
cpugraph = awful.widget.graph()
cpugraph:set_width(1920/4)
cpugraph:set_height(widget_height)
cpugraph:set_background_color("#494B4F")
cpugraph:set_color("#FF5656")
cpugraph:set_gradient_colors({ "#FF5656", "#88A175", "#AECF96" })
cpugraph_tooltip = awful.tooltip({ objects = { cpugraph.widget },})
vicious.register(
	cpugraph, 
	vicious.widgets.cpu,
	function (widget, args)
		cpugraph_tooltip:set_text(table.concat(args, ", "))
		return (args[1] + args[2] + args[3]) / 3
	 end, 
	 0.5)
widgets.cpu = cpugraph.widget
widgets.cpu:buttons(
	awful.util.table.join(
		awful.button({}, 1, function () 
			awful.util.spawn (taskapp) 
		end)))

mpdwidget = widget({ type = "textbox" })
vicious.register(mpdwidget, vicious.widgets.mpd,
    function (widget, args)
        if args["{state}"] == "Stop" then 
            return " - "
        else 
            return args["{Artist}"]..' - '.. args["{Title}"]
        end
    end, 10)

widgets.pkg = widget({type="textbox"})
widgets.pkg.width = 50
vicious.register(widgets.pkg, vicious.widgets.pkg, 
	"<span color='gray'> Upd: </span> $1 ", 1801, "Ubuntu")


widgets.weather = widget({ type = "textbox" })
sunny = '☀'
cloudy = '☁'
rainy = '☔'
snowy = '☃'
clear = '★'
weathertooltip = awful.tooltip({ objects = { widgets.weather },})
    vicious.register(widgets.weather, vicious.widgets.weather,
    function (widget, args)
    	weathertooltip:set_text("Stuff")
        if args["{tempf}"] == "N/A" then
            return " No Info "
        else
            return "<span color='#aaaaaa'>" .. string.lower(args["{sky}"]) .. ", " .. args["{tempf}"] .. "°F" .. "</span> "
        end
    end, 1200, "KAUS" )  -- Austin-Bergstrom Intl Airport
widgets.weather:buttons(
	awful.util.table.join(
		awful.button({}, 3, function () awful.util.spawn ( 
			browser .. " http://www.wunderground.com/US/" .. state .. "/" .. city .. ".html") end)))


widget_list = {
        -- right to left.
        -- mylayoutbox[s],
        widgets.date,
        separator, widgets.weather,
        separator, widgets.pkg,
        separator, widgets.vol,
        separator, widgets.net,
        separator, widgets.fs,
        separator, widgets.mem,
        separator, widgets.cpu,
        -- separator, pulseicon, pulsewidget, pulsebox,
        -- mpdwidget,
        -- widgets.mem,
        -- mytextclock,
	}
