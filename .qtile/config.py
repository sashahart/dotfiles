import logging
import subprocess

from libqtile.config import Key, Screen, Group, Match, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

# Define MOD as super - many keybindings will use this.
MOD = "mod4"


terminal = "xterm"
browser = "firefox"
music_player = "gmpc"
volume = "pavucontrol"
printer = "system-config-printer"


keys = [
    # Switch between windows in current stack pane
    Key([MOD], "k", lazy.layout.down()),
    Key([MOD], "j", lazy.layout.up()),

    # (Same, but more "normal" keys - I don't use alt-tab for other things)
    Key(["mod1"], "Tab", lazy.layout.down()),
    Key(["mod1", "shift"], "Tab", lazy.layout.up()),

    # Reorder windows up or down within current stack
    Key([MOD, "control"], "k", lazy.layout.shuffle_down()),
    Key([MOD, "control"], "j", lazy.layout.shuffle_up()),

    # Swap panes of split stack
    Key([MOD], "space", lazy.layout.rotate()),

    # Split or unsplit one side of the stack.
    #   split: all windows in stack shown at once.
    #   unsplit: one window from stack shown at once.
    # n.b.: this is really very useful
    Key([MOD, "shift"], "Return", lazy.layout.toggle_split()),

    # used for Columns layout
    Key([MOD, "shift"], "j", lazy.layout.shuffle_down()),
    Key([MOD, "shift"], "k", lazy.layout.shuffle_up()),
    Key([MOD, "shift"], "h", lazy.layout.shuffle_left()),
    Key([MOD, "shift"], "l", lazy.layout.shuffle_right()),


    Key([MOD], "Return", lazy.spawn(terminal)),
    Key(["control", "mod1"], "t", lazy.spawn(terminal)),

    # Toggle between different layouts as defined below
    Key([MOD], "Tab", lazy.next_layout()),

    Key([MOD], "w", lazy.window.kill()),

    Key([MOD, "control"], "r", lazy.restart()),
    Key([MOD, "control"], "q", lazy.shutdown()),
    Key([MOD], "semicolon", lazy.spawncmd()),

    # Cycle through groups with Ctrl-Alt-Left/Right
    Key(["mod1", "control"], "Left", lazy.screen.prevgroup(skip_managed=True)),
    Key(["mod1", "control"], "Right", lazy.screen.nextgroup(skip_managed=True)),

    Key([MOD], "slash", lazy.switchgroup()),

    # for tree layout
    Key([MOD], "h", lazy.layout.collapse_branch()),
    Key([MOD], "l", lazy.layout.expand_branch()),


    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10")),
]

# Groups are workspaces. Each window belongs to one Group.
# This name is used directly by qtile.
# (the defaults like asdf are hard for me to chord with super. Maybe my hands
# are too big.)
groups = [
    Group(i) for i in "1234567890"
]
groups.extend([
    Group("m", matches=[
        Match(wm_class=["gmpc", "Gmpc"])
    ]),
    Group("g", matches=[
        Match(wm_class=["Steam"])
    ]),
])

for group in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([MOD], group.name, lazy.group[group.name].toscreen()),
        # mod1 + shift + letter of group =
        # switch to & move focused window to group
        Key([MOD, "shift"], group.name, lazy.window.togroup(group.name)),
    ])

TEXT_OPTIONS = {
    "font": "Liberation Sans",
    "foreground": "AAAAAA",
    "background": "1D1D1D",
    "fontsize": 16,
    "padding": 3,
}

LAYOUT_OPTIONS = {
    "border_normal": "#444444",
    "border_focus": "#888888",
    "border_width": 1,
}

# Layouts are algorithms for arranging windows on a Screen.
# Here's where we configure layouts to cycle through;
# the key for cycling through them is separate
layouts = [
    layout.Columns(),

    # Stack is the Qtile default layout:
    # Vertical columns, each can either show one window
    # or tile all the windows in that stack (split?)
    # according to keybinds.
    # However, it doesn't seem to have ratio resizing?
    # layout.Stack(num_stacks=3),

    # One window at a time, "maximized".
    layout.Max(),

    # like Max with a sort of task bar on one side.
    # layout.TreeTab(panel_width=256, fontsize=12),

    # Main pane on one side, secondary panes on other,
    # but no split/unsplit so why?
    # layout.MonadTall(**layout_options),

    # Try to set all windows to tiles with same w/h ratio,
    # like the Brady Bunch.
    # or a poor man's Expose
    layout.RatioTile(**layout_options),

    # layout.Tile(ratio=0.25, **layout_options),

    # One active Window, sorta thumbnails at right.
    # layout.Zoomy(**layout_options),

    # Tile - no docstring.
    # layout.Slice()  # fails?
]

sep_color = "444444"
widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

# Screen objects represent physical screens,
# this is where widgets are configured
screens = [
    Screen(
        bottom=bar.Bar([
                # Room for system/notification icons
                widget.Systray(),

                # Tells the name of what has focus
                # but not compatible with TaskList
                # widget.WindowName(**TEXT_OPTIONS),

                # Can't have a WindowName also because that's a STRETCH
                # widget and you can only have one of those.
                widget.TaskList(**TEXT_OPTIONS),

                # Indicate what's on clipboard
                # widget.Clipboard(max_width=5, timeout=2, **TEXT_OPTIONS),

                # Prompt for things like spawning processes or switching
                # groups, usually invisible
                widget.Prompt(**TEXT_OPTIONS),

                # Name the currently applied layout - can click to switch
                widget.CurrentLayout(**TEXT_OPTIONS),

                # List of workspaces
                widget.GroupBox(
                    urgent_alert_method="text",
                    this_current_screen_border="444444",
                    **TEXT_OPTIONS
                ),

                widget.Volume(),

                widget.BatteryIcon(),

                widget.Sep(foreground=sep_color),

                widget.Clock(
                    format="%Y-%m-%d %a %I:%M %p",
                    **TEXT_OPTIONS
                ),

                # widget.Sep(foreground=sep_color),
                # widget.TextBox("default config", name="default"),

                # # Does not seem to work
                # widget.YahooWeather(
                #     woeid="12590014",
                #     metric=False,
                #     format="{condition_text}, {condition_temp}°", **TEXT_OPTIONS
                # ),
            ],
            # 30px high
            30,
        ),
    ),
    Screen(
        bottom=bar.Bar([
            widget.GroupBox(**TEXT_OPTIONS)
        ], 30)
    )
]

# Define mouse actions
mouse = [
    # Drag floating layouts.
    Drag([MOD], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([MOD], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([MOD], "Button2", lazy.window.bring_to_front())
]

# Let some things just use the floating layout
@hook.subscribe.client_new
def floating_dialogs(window):
    dialog = window.window.get_wm_type() == "dialog"
    transient = window.window.get_wm_transient_for()
    # if "zim" in window.window.get_wm_class():
    #     window.floating = False
    if dialog or transient:
        window.floating = True


@hook.subscribe.startup
def on_startup():
    pass


def main(qtile):
    # set logging level
    qtile.cmd_info()

dgroups_key_binder = None
dgroups_app_rules = []
# I commented this out before? to define main I think
# main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    {"wmclass": "confirm"},
    {"wmclass": "dialog"},
    {"wmclass": "download"},
    {"wmclass": "error"},
    {"wmclass": "file_progress"},
    {"wmclass": "notification"},
    {"wmclass": "splash"},
    #{"wmiconname": "Warning"},
    {"wmclass": "toolbar"},
    # inkscape
    {"wmclass": "Inkscape"},
    # gitk
    {"wmclass": "confirmreset"},
    {"wmclass": "makebranch"},
    {"wmclass": "maketag"},
    {"wname": "branchdialog"},
    # gpg key password entry
    {"wname": "pinentry"},
    # ssh gui
    {"wmclass": "ssh-askpass"},
])

auto_fullscreen = True
focus_on_window_activation = "smart"

# Use a false wmname that's on a java whitelist to trick java UI toolkits into
# working.
wmname = "LG3D"

# For running inside Gnome
import subprocess
import os
from libqtile import hook

# Try to make qtile cooperate with gnome
@hook.subscribe.startup
def dbus_register():
    id = os.environ.get("DESKTOP_AUTOSTART_ID")
    if not id:
        return
    subprocess.Popen([
        "dbus-send",
        "--session",
        "--print-reply",
        "--dest=org.gnome.SessionManager",
        "/org/gnome/SessionManager",
        "org.gnome.SessionManager.RegisterClient",
        "string:qtile",
        "string:" + id
    ])
